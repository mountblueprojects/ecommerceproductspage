function getDetails(url) {
  fetch(url)
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error(response.status);
      }
    })

    .then((data) => {
      document.querySelector(".preload").style.display = "none";
      const products = document.querySelector(".cards");

      if (data.length === 0) {
        products.style.display = "none";
        const noProducts = document.createElement("div");
        noProducts.setAttribute("class", "empty-products");
        noProducts.innerText = "Sorry! No Products Found";
        noProducts.style.fontSize = "3rem";
        noProducts.style.height = "90vh";
        noProducts.style.display = "flex";
        noProducts.style.justifyContent = "center";
        noProducts.style.alignItems = "center";

        document.querySelector("article").append(noProducts);
      } else {
        for (let index = 0; index < data.length; index++) {
          const product = document.createElement("div");

          product.innerHTML = `<div class="options">
        <img class="zoom" src="./images/zoom.png" onclick="getProduct(${data[index].id})"/>
        <img class="bag" src="./images/8109.png" />
      </div>

      <div class="product" onclick="getProduct(${data[index].id})"><img  src="${data[index].image}" /></div>
      <div class="details">
            <div class="name" onclick="getProduct(${data[index].id})">${data[index].title}</div>
            <br>
            <div class="price" onclick="getProduct(${data[index].id})">$${data[index].price}</div>
            <br>
            <div class="description" onclick="getProduct(${data[index].id})">${data[index].description}</div>
            <br>
            <div class="category" onclick="getProduct(${data[index].id})">${data[index].category}</div>
            <br>
            <div onclick="getProduct(${data[index].id})"><i class="fa-solid fa-star"></i>${data[index].rating.rate} (${data[index].rating.count})</div>
          </div>`;
          product.setAttribute("class", "card");
          products.append(product);
        }
      }
    })

    .catch((err) => {
      const offline = document.createElement("div");
      offline.innerText = "An error occurred while fetching products";
      offline.setAttribute("class", "offline");
      products.style.display = "none";

      document.querySelector("article").append(offline);
    });
}

function getProduct(id) {
  fetch("https://fakestoreapi.com/products/" + id)
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        console.log("No Products");
      }
    })

    .then((data) => {
      const product = document.createElement("div");
      const products = document.querySelector(".cards");
      const singleCard = document.querySelector(".singleCard");
      products.style.display = "none";
      singleCard.style.display = "flex";
      product.innerHTML = `<div class="product"><img  src="${data.image}" /></div>
          <div class="details">          
                <div class="name">${data.title}</div>
                <div class="price">$${data.price}</div>
                <div class="description">${data.description}</div>
                <div><i class="fa-solid fa-star"></i>${data.rating.rate} (${data.rating.count})</div>
              </div>`;
      product.setAttribute("class", "card");
      document.querySelector(".back i").style.display = "block";
      document.querySelector("article").style.gap = "0";
      singleCard.append(product);
    })

    .catch((err) => {
      console.log(err);
    });
}

function getCategories(url) {
  fetch(url + "/categories")
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        console.log("No Products");
      }
    })

    .then((data) => {
      const categories = document.querySelector("#categories");
      for (let index = 0; index < data.length; index++) {
        const category = document.createElement("div");
        category.innerText = `${data[index]}`;
        category.setAttribute("class", `${data[index]}`);
        categories.append(category);
      }
    })

    .catch((err) => {
      console.log(err);
    });
}

const loader = document.querySelector(".emoji");

const load = (arr) => {
  setInterval(() => {
    loader.innerText = arr[Math.floor(Math.random() * arr.length)];
  }, 125);
};

function refresh() {
  window.location.reload();
}

load(["🕐", "🕜", "🕑", "🕝", "🕒", "🕞"]);
getDetails("https://fakestoreapi.com/products");
getCategories("https://fakestoreapi.com/products");
