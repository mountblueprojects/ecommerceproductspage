function saveUserData() {
  window.localStorage.setItem(
    "firstname",
    document.getElementById("first-name").value
  );
  window.localStorage.setItem(
    "lastname",
    document.getElementById("last-name").value
  );
  window.localStorage.setItem(
    "email",
    document.getElementById("email-id").value
  );
  window.localStorage.setItem(
    "password",
    document.getElementById("password").value
  );
}

function validate(event) {
  event.preventDefault();

  const firstName = document.getElementById("first-name");
  const lastName = document.getElementById("last-name");
  const emailId = document.getElementById("email-id");
  const password = document.getElementById("password");
  const confirmPassword = document.getElementById("re-enter-password");
  const check = document.getElementById("terms");

  let errorMessages = document.getElementsByClassName("error-message");
  let errorFlag = 0;

  while (errorMessages.length > 0) {
    errorMessages[0].parentNode.removeChild(errorMessages[0]);
  }

  if (
    firstName.value.trim().length === 0 ||
    !/^[a-zA-Z]+$/.test(firstName.value)
  ) {
    errorFlag = 1;
    const error = document.createElement("p");

    error.innerText = "Please enter a valid First Name with characters only";
    error.setAttribute("style", "color:red;font-size:1rem");
    error.setAttribute("class", "error-message");
    document
      .getElementById("first-name")
      .insertAdjacentElement("afterend", error);
  }

  if (
    lastName.value.trim().length === 0 ||
    !/^[a-zA-Z]+$/.test(lastName.value)
  ) {
    errorFlag = 1;
    const error = document.createElement("p");

    error.innerText = "Please enter a valid Last Name with characters only";
    error.setAttribute("style", "color:red;font-size:1rem");
    error.setAttribute("class", "error-message");
    document
      .getElementById("last-name")
      .insertAdjacentElement("afterend", error);
  }

  if (
    emailId.value.trim().length === 0 ||
    !/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(emailId.value)
  ) {
    errorFlag = 1;
    const error = document.createElement("p");

    error.innerText = "Please enter a valid e-mail address";
    error.setAttribute("style", "color:red;font-size:1rem");
    error.setAttribute("class", "error-message");
    document
      .getElementById("email-id")
      .insertAdjacentElement("afterend", error);
  }

  if (password.value.trim().length === 0) {
    errorFlag = 1;
    const error = document.createElement("div");

    error.innerHTML = `<p>Password should contain alphabets</p>
    <p>Password should contain digits</p>
    <p>Password should contain special characters</p>`;

    error.setAttribute("style", "color:red;font-size:1rem");
    error.setAttribute("class", "error-message");

    document
      .getElementById("password")
      .insertAdjacentElement("afterend", error);
  } else if (!/[@$!%*#?&]/.test(password.value) && !/\d/.test(password.value)) {
    errorFlag = 1;
    const error = document.createElement("p");

    error.innerHTML = `<p>Password should contain digits</p>
    <p>Password should contain special characters</p>`;

    error.setAttribute("style", "color:red;font-size:1rem");
    error.setAttribute("class", "error-message");
    document
      .getElementById("password")
      .insertAdjacentElement("afterend", error);
  } else if (!/\d/.test(password.value) && !/[a-zA-Z]/.test(password.value)) {
    errorFlag = 1;
    const error = document.createElement("p");

    error.innerHTML = `<p>Password should contain alphabets</p>
    <p>Password should contain digits</p>`;

    error.setAttribute("style", "color:red;font-size:1rem");
    error.setAttribute("class", "error-message");
    document
      .getElementById("password")
      .insertAdjacentElement("afterend", error);
  } else if (
    !/[@$!%*#?&]/.test(password.value) &&
    !/[a-zA-Z]/.test(password.value)
  ) {
    errorFlag = 1;
    const error = document.createElement("p");

    error.innerHTML = `<p>Password should contain alphabets</p>
    <p>Password should contain special characters</p>`;

    error.setAttribute("style", "color:red;font-size:1rem");
    error.setAttribute("class", "error-message");
    document
      .getElementById("password")
      .insertAdjacentElement("afterend", error);
  } else if (!/\d/.test(password.value)) {
    errorFlag = 1;
    const error = document.createElement("p");

    error.innerText = "Password should contain digits";

    error.setAttribute("style", "color:red;font-size:1rem");
    error.setAttribute("class", "error-message");
    document
      .getElementById("password")
      .insertAdjacentElement("afterend", error);
  } else if (!/[a-zA-Z]/.test(password.value)) {
    errorFlag = 1;
    const error = document.createElement("p");

    error.innerText = "Password should contain alphabets";

    error.setAttribute("style", "color:red;font-size:1rem");
    error.setAttribute("class", "error-message");
    document
      .getElementById("password")
      .insertAdjacentElement("afterend", error);
  } else if (!/[@$!%*#?&]/.test(password.value)) {
    errorFlag = 1;
    const error = document.createElement("p");

    error.innerText = "Password should contain special characters";

    error.setAttribute("style", "color:red;font-size:1rem");
    error.setAttribute("class", "error-message");
    document
      .getElementById("password")
      .insertAdjacentElement("afterend", error);
  } else {
    errorFlag = 0;
  }

  if (confirmPassword.value !== password.value) {
    errorFlag = 1;
    const error = document.createElement("p");

    error.innerText = "Passwords does not match";

    error.setAttribute("style", "color:red;font-size:1rem");
    error.setAttribute("class", "error-message");
    document
      .getElementById("re-enter-password")
      .insertAdjacentElement("afterend", error);
  }

  if (!check.checked) {
    errorFlag = 1;
    const error = document.createElement("p");

    error.innerText = "Please accept the terms and conditions";

    error.setAttribute("style", "color:red;font-size:1rem");
    error.setAttribute("class", "error-message");
    document
      .getElementById("terms-label")
      .insertAdjacentElement("afterend", error);
  }

  if (errorFlag === 0) {
    let formBox = document.getElementById("form-box");
    let mainBox = document.getElementById("main-box");
    saveUserData();

    formBox.style.display = "none";
    const exit = document.createElement("p");
    exit.innerHTML = `<p>Signup Successful</p>
    <p>Redirecting to Homepage in 3s</p>`;
    exit.style.textAlign = "center";
    mainBox.append(exit);
    setTimeout(() => {
      window.location = "/";
    }, 3000);
  }
}

function exist() {
  if (localStorage.getItem("firstname")) {
    let header = document.getElementById("h1");
    let formBox = document.getElementById("form-box");
    let button = document.getElementById("logout");
    formBox.style.display = "none";
    header.innerText = `Welcome ${localStorage.getItem(
      "firstname"
    )} ${localStorage.getItem("lastname")}`;
    button.style.display = "flex";
  }
}

function logout() {
  localStorage.clear();
  window.location = "/";
}

exist();
